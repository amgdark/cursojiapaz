# Comandos Docker

Crea los servicios en función de las imágenes configuradas, --build, crea una imagen nueva a partir de un Dockerfile
```
docker-compose up -d --build
```

Ingresa al contenedor del servicio app.+
```
docker-compose exec app bash
```

Muestra los logs del contenedor del servicio app
```
docker-composer logs app
```

Detiene y elimina los contenedores de los servicios
```
docker-composer down
```

Detiene y elimina los contenedores de los servicios, además de eliminar los volúmenes y redes activas.
```
docker-composer down -v
```
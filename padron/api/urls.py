from django.urls import path
from . import views

urlpatterns = [
    path('localidades/', views.localidad_list),
    path('localidades/<int:pk>/', views.localidad_detail),
]
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from usuarios.models import Localidad
from .serializers import LocalidadSerializerModel
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated

@api_view(['GET', 'POST'])
# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
def localidad_list(request):
    if request.method == 'GET':
        localidad = Localidad.objects.all()
        serializer = LocalidadSerializerModel(localidad, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = LocalidadSerializerModel(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
def localidad_detail(request, pk):
    try:
        localidad = Localidad.objects.get(pk=pk)
    except Localidad.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = LocalidadSerializerModel(localidad)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = LocalidadSerializerModel(localidad, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        localidad.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
# @csrf_exempt
# def localidad_list(request):
#     if request.method == 'GET':
#         localidades = Localidad.objects.all()
#         serializer = LocalidadSerializerModel(localidades, many=True)
#         return JsonResponse(serializer.data, safe=False)

#     elif request.method == 'POST':
#         data = JSONParser().parse(request)
#         serializer = LocalidadSerializerModel(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=201)
#         return JsonResponse(serializer.errors, status=400)
    
# @csrf_exempt
# def localidad_detail(request, pk):
#     try:
#         localidad = Localidad.objects.get(pk=pk)
#     except Localidad.DoesNotExist:
#         return HttpResponse(status=404)

#     if request.method == 'GET':
#         serializer = LocalidadSerializerModel(localidad)
#         return JsonResponse(serializer.data)

#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         serializer = LocalidadSerializerModel(localidad, data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors, status=400)

#     elif request.method == 'DELETE':
#         localidad.delete()
#         return HttpResponse(status=204)
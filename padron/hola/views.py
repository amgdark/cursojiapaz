from django.shortcuts import render

def hola_func(request):
    context = {
        'nombre': 'Alex',
        'edad': 41,
        'materias': [
            'Linux',
            'Pruebas',
            'Deployment',
            'Administración'
        ],
        'calificaciones':[
            {'id_materia':1, 'calif':9},
            {'id_materia':2, 'calif':8},
            {'id_materia':3, 'calif':7},
            {'id_materia':4, 'calif':10},
        ]
        
    }
    return render(request, 'hola.html', context)

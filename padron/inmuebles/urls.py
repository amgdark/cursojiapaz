from django.urls import path
from inmuebles import views


app_name = 'inmuebles'

urlpatterns = [
    path('', views.lista_inmuebles, name='lista'),
    path('nuevo', views.nuevo_inmueble, name='nuevo'),
    # path('eliminar/<int:pk>', views_localidades.EliminarLocalidad.as_view(), name='eliminar'),
    # path('editar/<int:pk>', views_localidades.EditarLocalidad.as_view(), name='editar'),
    # path('datalle/<int:pk>', views_localidades.DetalleLocalidad.as_view(), name='detalle'),
    
    # path('colonias', views_localidades.busca_colonias, name='busca_colonias'),
    
]


# jiapaz.gob.mx/usuarios
# jiapaz.gob.mx/usuarios/nuevo
# jiapaz.gob.mx/usuarios/eliminar/1
# jiapaz.gob.mx/usuarios/
# jiapaz.gob.mx/hola
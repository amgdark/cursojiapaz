from django.contrib import admin
from django.urls import path, include
from hola.views import hola_func
from usuarios.views import PaginaPrincipal
from django.conf import settings
from django.conf.urls.static import static


from rest_framework.authtoken import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hola/', hola_func),
    path('usuarios/', include('usuarios.urls')),
    path('localidades/', include('usuarios.urls_loc')),
    path('inmuebles/', include('inmuebles.urls')),
    
    path('', PaginaPrincipal.as_view(), name='principal'),
    
    
    path('api/', include('api.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    
    # path('api-token-auth/', views.obtain_auth_token),
    
    path('api/auth/', include('knox.urls'))
    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# jiapaz.gob.mx/usuarios
# jiapaz.gob.mx/usuarios/nuevo
# jiapaz.gob.mx/usuarios/eliminar/1
# jiapaz.gob.mx/usuarios/lista

# jiapaz.gob.mx/localidades
# jiapaz.gob.mx/localidades/nuevo
# jiapaz.gob.mx/localidades/eliminar/1
# jiapaz.gob.mx/localidades/editar/1

# jiapaz.gob.mx/hola


class Calculadora:
    def sumar(self, num1, num2, *args):
        # print(len(args))
        if len(args) > 0:
            return 'Solo se aceptan 2 parametros'
        if isinstance(num1, int) and isinstance(num2, int):
            return num1 + num2
        elif isinstance(num1, float) and isinstance(num2, float):
            return num1 + num2
        else:
            return 'Solo se admiten numeros'
        
    def restar(self, num1, num2):
        return num1-num2

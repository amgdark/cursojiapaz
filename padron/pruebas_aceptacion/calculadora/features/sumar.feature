Característica: Sumar dos números
    Como usuario del sistema de calculadora
    Quiero sumar dos números
    Para hacer cálculos precisos

        Escenario: Sumar 4 más 3
            Dado que ingreso los números "4" y "3"
             Cuando realizo el cálculo
             Entonces puedo ver el resultado "7"

        Escenario: Suma con cero
            Dado que ingreso los números "0" y "10"
             Cuando realizo el cálculo
             Entonces puedo ver el resultado "10"

        Escenario: Suma con número negativo
            Dado que ingreso los números "-4" y "10"
             Cuando realizo el cálculo
             Entonces puedo ver el resultado "6"

        Escenario: Sumar con un caracter
            Dado que ingreso el caracter "X" y el número "10"
             Cuando realizo el cálculo
             Entonces puedo ver el mensaje "Solo se admiten numeros"

        Escenario: Suma con números decimales
            Dado que ingreso los números decimales "7.2" y "3.2"
             Cuando realizo el cálculo
             Entonces puedo ver el resultado en decimal "10.4"
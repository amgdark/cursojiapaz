Característica: Agrega colonia
    Como administrador del sistema de Padrón
    Quiero agregar una Colonia 
    Para tener actualizado mi inventario de colonias.


        Escenario: Datos correctos de colonia
            Dado que ingreso a la dirección "http://localhost:8000/usuarios/login"
              Y que tecleo el usuario "alex" y el password "alexmau1"
              Y presiono el botón de Iniciar sesión
              Y me voy a dirección "http://localhost:8000/admin"
              Y doy clic en el link "Colonias"
              Y nuevamente doy click en el botón "AGREGAR COLONIA"
              Y tecleo en Nombre "Colinia de cañitas" y en Localidad "Cañitas"
             Cuando presiono el botón "GUARDAR"
             Entonces puedo el mensaje "Colinia de cañitas"
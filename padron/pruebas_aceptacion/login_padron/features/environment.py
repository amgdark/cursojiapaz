from behave import fixture, use_fixture
# from behave4my_project.fixtures import wsgi_server
from selenium import webdriver
from django.test import TestCase

@fixture
def selenium_browser_chrome(context):
    options = webdriver.ChromeOptions()
    options.add_argument("--window-size=1600,900")
        
    context.driver = webdriver.Chrome(options=options)
    context.test = TestCase()
    yield context.driver
    # -- CLEANUP-FIXTURE PART:
    context.driver.quit()

def before_all(context):
    # use_fixture(wsgi_server, context, port=8000)
    use_fixture(selenium_browser_chrome, context)
    # -- HINT: CLEANUP-FIXTURE is performed after after_all() hook is called.

# def before_feature(context, feature):
#     model.init(environment='test')
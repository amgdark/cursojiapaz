Característica: Acceso al sistema
    Como usuario del sistema Padrón
    Quiero iniciar sesión
    Para realizar mis actividades dentro del sistema


        Escenario: Credenciales correctas
            Dado que ingreso a la dirección "http://localhost:8000/usuarios/login"
              Y que tecleo el usuario "alex" y el password "alexmau1"
             Cuando presiono el botón de Iniciar sesión
             Entonces puedo ver el mensaje "Bienvenido !!!"

        Escenario: Credenciales incorrectas
            Dado que ingreso a la dirección "http://localhost:8000/usuarios/login"
              Y que tecleo el usuario "alex" y el password "admin123"
             Cuando presiono el botón de Iniciar sesión
             Entonces puedo ver el mensaje de error "Verifica tus credenciales; el usuario no existe"
import os


def edad(edad_param):
    if edad_param < 0:
        return 'No existes'
    elif edad_param < 13:
        return 'Eres niño'
    elif edad_param < 18:
        return 'Eres adolescente'
    elif edad_param < 65:
        return 'Eres adulto'
    elif edad_param < 120:
        return 'Eres adulto mayor'

    return 'Eres Mumm-Ra'

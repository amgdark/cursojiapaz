import unittest
from edades import edad

class TestEdades(unittest.TestCase):
    
    def test_menor_de_cero(self):
        self.assertEqual(edad(-1), 'No existes', 'La prueba falla')
      
    def test_menor_de_trece(self):
        self.assertEqual(edad(10), 'Eres niño', 'La prueba falla')
    
    def test_menor_de_dieciocho(self):
        self.assertEqual(edad(15), 'Eres adolescente', 'La prueba falla')
    
    def test_menor_de_sesenta_y_cinco(self):
        self.assertEqual(edad(35), 'Eres adulto', 'La prueba falla')
    
    def test_menor_de_ciento_veinte(self):
        self.assertEqual(edad(85), 'Eres adulto mayor', 'La prueba falla')
    
    def test_eres_mumm_ra(self):
        self.assertEqual(edad(130), 'Eres Mumm-Ra', 'La prueba falla')
    
    def test_edad_cero(self):
        self.assertEqual(edad(0), 'Eres niño', 'La prueba falla')


if __name__ == '__main__': # pragma: no cover
    unittest.main()
$('#id_localidad').change(function(e){
    let token = $('[name="csrfmiddlewaretoken"]').val();
    let url = '/localidades/colonias';

    $.ajax({
        type: 'POST',
        url: url,
        data: {'id':$(this).val(), 'csrfmiddlewaretoken': token},
        success: function(data){
            let html= '';
            $.each(data, function(llave, valor){
                html+=`<option value="${valor.id}">${valor.nombre}</option>`
            });
            $('#id_colonia').html(html); 
        },
    });

});
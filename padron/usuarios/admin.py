from django.contrib import admin
from usuarios.models import Usuario, Localidad, Colonia

admin.site.register(Usuario)
admin.site.register(Localidad)
admin.site.register(Colonia)

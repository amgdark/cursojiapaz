from django import forms
from usuarios.models import Usuario, Localidad, EDO_CIVIL, GENERO
from django.contrib.auth.models import User


class FormUsuario(forms.ModelForm):
    
    class Meta:
        model = Usuario
        fields = [
            'derivada', 
            'inmueble', 
            'toma', 
            'nombre',
            'apellido_paterno',
            'apellido_materno',
            'curp',
            'localidad',
            'colonia',
            'estado_civil',
            'genero',
            'foto',
            'archivo',
        ]
        # fields = '__all__'
        # exclude = ['toma','inmueble']
        
        widgets = {
            'inmueble':forms.NumberInput(attrs={'class':'form-control'}),
            'derivada':forms.NumberInput(attrs={'class':'form-control'}),
            'toma':forms.NumberInput(attrs={'class':'form-control'}),
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'apellido_paterno':forms.TextInput(attrs={'class':'form-control'}),
            'apellido_materno':forms.TextInput(attrs={'class':'form-control'}),
            'curp':forms.TextInput(attrs={'class':'form-control'}),
            'localidad':forms.Select(attrs={'class':'form-control'}),
            'colonia':forms.Select(attrs={'class':'form-control'}),
            'estado_civil':forms.Select(attrs={'class':'form-control'}),
            'genero':forms.Select(attrs={'class':'form-control'}),
        }
        
class FormUser(forms.ModelForm):
    username = forms.CharField(label='Número de empleado', 
        widget=forms.TextInput(attrs={'class':'form-control'}))
    class Meta:
        model = User
        fields = ['username','email']
        widgets = {
            'email':forms.EmailInput(attrs={'class':'form-control'}),
        }
        
    
class FormLocalidad(forms.ModelForm):
    
    class Meta:
        model = Localidad
        fields = '__all__'
        widgets = {
            'nombre':forms.TextInput(attrs={'class':'form-control','placeholder':'Escribe tu nombre'}),
            'descripcion':forms.Textarea(attrs={'class':'form-control'}),
        }
        
class FormBusqueda(forms.Form):
    nombre = forms.CharField(
        max_length=100, 
        required=False, 
        widget=forms.TextInput(attrs={'placeholder':'Nombre','class':'form-control'})
    )
    apellido_paterno = forms.CharField(
        max_length=100, 
        required=False,
        widget=forms.TextInput(attrs={'placeholder':'Apellido paterno','class':'form-control'})
    )
    apellido_materno = forms.CharField(
        max_length=100, 
        required=False,
        widget=forms.TextInput(attrs={'placeholder':'Apellido materno','class':'form-control'})
    )
    genero = forms.CharField(
        max_length=1, 
        required=False, 
        widget=forms.Select(choices=GENERO, attrs={'class':'form-control'})
    )
    estado_civil = forms.CharField(
        max_length=1, 
        required=False, 
        widget=forms.Select(choices=EDO_CIVIL, attrs={'class':'form-control'})
    )
    localidad = forms.CharField(
        max_length=100, 
        required=False, 
        widget=forms.TextInput(attrs={'placeholder':'Localidad','class':'form-control'})
    )
    
    
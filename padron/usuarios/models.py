from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator
from usuarios.validators import curp_validator


EDO_CIVIL = (
    ('', '-------'),
    ('1', 'Soltero(a)'),
    ('2', 'Casado(a)'),
    ('3', 'Divorciado(a)'),
    ('4', 'Viudo(a)'),
)

GENERO = (
    ('', '-------'),
    ('1', 'Femenino'),
    ('2', 'Masculino'),
    ('3', 'No binario'),
)

class Usuario(models.Model):
    curp = models.CharField('CURP', max_length=18, 
        validators=[
            MinLengthValidator(16, 'La longitud minima es de 16 caracteres'),
            curp_validator
        ]
    )
    inmueble = models.BigIntegerField()
    derivada = models.BigIntegerField()
    toma = models.SmallIntegerField()
    nombre = models.CharField(max_length=100)
    apellido_materno = models.CharField(max_length=100)
    apellido_paterno = models.CharField(max_length=100)
    localidad = models.ForeignKey("usuarios.Localidad", verbose_name='Localidad', on_delete=models.DO_NOTHING)
    estado_civil = models.CharField('Estado civil', max_length=1, choices=EDO_CIVIL)
    genero = models.CharField('Sexo', max_length=1, choices=GENERO)
    colonia = models.ForeignKey("usuarios.Colonia", verbose_name="Colonia", on_delete=models.DO_NOTHING)
    user = models.OneToOneField(User, verbose_name="Usuario", related_name='usuario_user', on_delete=models.CASCADE)
    foto = models.ImageField(upload_to='perfiles',null=True, blank=True)
    archivo = models.FileField(upload_to='archivos',null=True, blank=True )
    
    def __str__(self):
        return f"{self.nombre} {self.apellido_paterno} {self.apellido_materno}" 
    
class Localidad(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField('Descripción', null=True, blank=True)
    
    def __str__(self):
        return self.nombre
    
class Colonia(models.Model):
    nombre = models.CharField(max_length=200)
    localidad = models.ForeignKey("usuarios.Localidad", verbose_name="Localidad", on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre


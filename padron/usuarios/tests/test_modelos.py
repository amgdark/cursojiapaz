from django.test import TestCase
from usuarios.models import Usuario, Localidad, Colonia
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from usuarios.facturacion import realizar_factura

class TestSmoke(TestCase):
    def get_localidad(self):
        return Localidad.objects.create(
            nombre = 'Guadalupe',
            descripcion = 'Descripción GPE'
        )
    def get_colonia(self):
        return Colonia.objects.create(
            nombre = 'Villas',
            localidad = self.get_localidad()
        )
        
    def get_user(self):
        return User.objects.create_user(
            username='111111111',
            password='juan123'
        )
    
    def get_usuario(self):
        return Usuario.objects.create(
            curp = 'MAGA801121HZSRNL07',
            inmueble = 3,
            derivada = 4,
            toma = 6,
            nombre = 'Juan',
            apellido_materno = 'Pérez',
            apellido_paterno = 'Ramos',
            localidad = self.get_localidad(),
            estado_civil = '1',
            genero = '1',
            colonia = self.get_colonia(),
            user = self.get_user(),
            foto = '/Users/alexmau/Downloads/img1.jpeg',
            archivo = '/Users/alexmau/Downloads/EstatutoGeneralUAZ.pdf',       
        )
        
    def test_suma_2_mas_2(self):
        self.assertEquals(2+2, 4)
        
    def test_inserta_usuario(self):
        self.get_usuario()
        self.assertEquals(Usuario.objects.count(), 1)
        self.assertEquals(Usuario.objects.first().nombre, 'Juan')
        
    def test_curp_invalido(self):
        usuario = self.get_usuario()
        usuario.curp = '123123'
        
        with self.assertRaises(ValidationError):
            usuario.full_clean()
            
    def test_inmueble_invalido(self):
        usuario = self.get_usuario()
        usuario.inmueble = 'x'
        
        with self.assertRaises(ValidationError):
            usuario.full_clean()
            
    def test_foto_vacia(self):
        usuario = self.get_usuario()
        usuario.foto = None
        usuario.save()
        
        self.assertEquals(usuario.foto, None)
        
    def test_username_requerido_mensaje(self):
        user = User(password='aasdasd')
        with self.assertRaisesMessage(ValidationError, 'Este campo no puede estar en blanco.'):
            user.full_clean()
            
    def test_inmueble_invalido_mensaje(self):
        usuario = self.get_usuario()
        usuario.inmueble = 'x'
        
        with self.assertRaisesMessage(ValidationError, '“x”: el valor debería ser un numero entero'):
            usuario.full_clean()

    def test_usuario_duplicado(self):
        self.get_user()
        usuario = User(
            username='111111111',
            password='juan123'
        )
        
        with self.assertRaisesMessage(ValidationError, 'Ya existe un usuario con ese nombre.'):
            usuario.full_clean()
            
    def test_email_incorrecto(self):
        usuario = self.get_user()
        usuario.email = 'jasg15@gmail'
        with self.assertRaises(ValidationError):
            usuario.full_clean()
            
    def test_factura_escenario_x(self):
        resultado = 2
        self.assertEquals(resultado, realizar_factura())